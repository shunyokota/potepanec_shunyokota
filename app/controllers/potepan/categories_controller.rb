module Potepan
  class CategoriesController < ApplicationController
    def show
      @taxonomies = Spree::Taxonomy.joins(taxons: 'products').distinct
      @taxon = Spree::Taxon.find_by(id: params[:id])
      @products = @taxon&.products || Spree::Product.all
    end
  end
end
