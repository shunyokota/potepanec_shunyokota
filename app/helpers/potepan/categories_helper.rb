module Potepan
  module CategoriesHelper
    GRID_LAYOUT = 'grid'.freeze
    LIST_LAYOUT = 'list'.freeze

    def products_layout
      params[:layout] ? params[:layout] : GRID_LAYOUT
    end

    def products_layout_grid?
      products_layout == GRID_LAYOUT
    end

    def products_layout_list?
      products_layout == LIST_LAYOUT
    end

    def potepan_categories_grid_layout_path(taxon = nil)
      if taxon
        potepan_categories_path(taxon.id, layout: GRID_LAYOUT)
      else
        potepan_categories_path(layout: GRID_LAYOUT)
      end
    end

    def potepan_categories_list_layout_path(taxon = nil)
      if  taxon
        potepan_categories_path(taxon.id, layout: LIST_LAYOUT)
      else
        potepan_categories_path(layout: LIST_LAYOUT)
      end
    end
  end
end
