require 'rails_helper'

RSpec.describe Potepan::CategoriesHelper, type: :helper do
  describe 'products_layoutの検証' do
    context 'パラメータからレイアウトが指定されていない' do
      it 'GRID_LAYOUTが返されること' do
        expect(products_layout).to eq Potepan::CategoriesHelper::GRID_LAYOUT
      end
    end

    context 'パラメータからレイアウトが指定されている' do
      let(:params) { { layout: 'test' } }
      it '指定された値が返されること' do
        expect(products_layout).to eq 'test'
      end
    end
  end

  describe 'products_layout_grid?の検証' do
    context 'グリッドレイアウトの場合' do
      let(:params) { { layout: Potepan::CategoriesHelper::GRID_LAYOUT } }
      it 'trueが返されること' do
        expect(products_layout_grid?).to eq true
      end
    end

    context 'リストレイアウトの場合' do
      let(:params) { { layout: Potepan::CategoriesHelper::LIST_LAYOUT } }
      it 'falseが返されること' do
        expect(products_layout_grid?).to eq false
      end
    end
  end

  describe 'products_layout_list?の検証' do
    context 'グリッドレイアウトの場合' do
      let(:params) { { layout: Potepan::CategoriesHelper::GRID_LAYOUT } }
      it 'falseが返されること' do
        expect(products_layout_list?).to eq false
      end
    end

    context 'リストレイアウトの場合' do
      let(:params) { { layout: Potepan::CategoriesHelper::LIST_LAYOUT } }
      it 'trueが返されること' do
        expect(products_layout_list?).to eq true
      end
    end
  end

  describe 'potepan_categories_grid_layout_pathの検証' do
    context 'taxonが指定されていない場合' do
      it 'categoriesのURLが返されること' do
        expect(potepan_categories_grid_layout_path).to eq potepan_categories_path(layout: Potepan::CategoriesHelper::GRID_LAYOUT)
      end
    end

    context 'taxonが指定されている場合' do
      let(:taxon) { create(:taxon) }
      it 'categories/:idのURLが返されること' do
        expect(potepan_categories_grid_layout_path(taxon)).to eq potepan_categories_path(taxon.id, layout: Potepan::CategoriesHelper::GRID_LAYOUT)
      end
    end
  end

  describe 'potepan_categories_list_layout_pathの検証' do
    context 'taxonが指定されていない場合' do
      it 'categoriesのURLが返されること' do
        expect(potepan_categories_list_layout_path).to eq potepan_categories_path(layout: Potepan::CategoriesHelper::LIST_LAYOUT)
      end
    end

    context 'taxonが指定されている場合' do
      let(:taxon) { create(:taxon) }
      it 'categories/:idのURLが返されること' do
        expect(potepan_categories_list_layout_path(taxon)).to eq potepan_categories_path(taxon.id, layout: Potepan::CategoriesHelper::LIST_LAYOUT)
      end
    end
  end
end
