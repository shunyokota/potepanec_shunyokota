require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe 'GET show' do
    let!(:taxon) do
      taxon1 = create(:taxon, name: "TAXON1")
      taxon2 = create(:taxon, name: "TAXON2")
      taxon3 = create(:taxon, name: "TAXON3") # productとのひも付きの無いレコード

      product1 = create(:product, name: "PRODUCT1")
      product2 = create(:product, name: "PRODUCT2")

      # taxonとproductのひも付きを登録
      classification1 = Spree::Classification.new
      classification1.taxon = taxon1
      classification1.product = product1
      classification1.save
      classification2 = Spree::Classification.new
      classification2.taxon = taxon2
      classification2.product = product2
      classification2.save

      # taxonomyのpositionを登録
      taxonomy1 = taxon1.taxonomy
      taxonomy1.position = 1
      taxonomy1.save
      taxonomy2 = taxon2.taxonomy
      taxonomy2.position = 2
      taxonomy2.save
      taxonomy3 = taxon3.taxonomy
      taxonomy3.position = 0
      taxonomy2.save

      Spree::Product.first.taxons.first
    end

    let!(:taxonomy) { Spree::Taxonomy.first }

    describe 'レスポンスの検証' do
      context 'パラメータで指定したtaxonが存在する' do
        it '200 OK を返すこと' do
          get :show, params: { id: taxon.id }
          expect(response.status).to eq(200)
        end
      end

      context 'パラメータからtaxonのidが指定されていない' do
        it '200 OK を返すこと' do
          get :show
          expect(response.status).to eq(200)
        end
      end

      context 'パラメータで指定したtaxonが存在しない' do
        it '200 OK を返すこと' do
          get :show, params: { id: 0 }
          expect(response.status).to eq(200)
        end
      end
    end

    describe 'Viewの検証' do
      it 'potepan/categories/showを描画すること' do
        get :show, params: { id: taxon.id }
        expect(response).to render_template :show
      end
    end

    describe 'モデルオブジェクトの検証' do
      describe '@taxonの検証' do
        context 'パラメータからtaxonのidが指定されている' do
          it '@taxonにパラメータで指定されたtaxonが設定されること' do
            get :show, params: { id: taxon.id }
            expect(assigns(:taxon)).to eq taxon
          end
        end

        context 'パラメータからtaxonのidが指定されていない' do
          it '@taxonにnilが設定されること' do
            get :show
            expect(assigns(:taxon)).to eq nil
          end
        end

        context 'パラメータから存在しないtaxonのidが指定されている' do
          it '@taxonにnilが設定されること' do
            get :show, params: { id: 0 }
            expect(assigns(:taxon)).to eq nil
          end
        end
      end

      describe '@productsの検証' do
        context 'パラメータからtaxonのidが指定されている' do
          it '@productsにパラメータで指定されたtaxonに紐づくproductのみが設定されること' do
            get :show, params: { id: taxon.id }
            expect(assigns(:products).size).to eq 1
          end
        end

        context 'パラメータからtaxonのidが指定されていない' do
          it '@productsに全てのproduct一覧が設定されること' do
            get :show
            expect(assigns(:products).size).to eq 2
          end
        end

        context 'パラメータから存在しないtaxonのidが指定されている' do
          it '@productsに全てのproduct一覧が設定されること' do
            get :show, params: { id: 0 }
            expect(assigns(:products).size).to eq 2
          end
        end
      end

      describe '@taxonomiesの検証' do
        it '@taxonomiesにpruductとのひも付きのあるレコード一覧が設定されること' do
          get :show
          expect(assigns(:taxonomies).size).to eq 2
        end

        it '@taxonomiesがpositionの昇順でソートされていること' do
          get :show
          expect(assigns(:taxonomies).first).to eq taxonomy
        end
      end
    end
  end
end
