require 'rails_helper'

RSpec.describe "categories/show.html.erb", type: :view do
  describe "@layoutでの商品表示レイアウト制御の検証" do
    let(:products) do
      create(:product, name: 'TAXON1')
      create(:product, name: 'TAXON2')
      Spree::Product.all
    end

    before do
      assign(:taxonomies, Spree::Taxonomy.none)
      assign(:products, products)
    end

    context "GRID_LAYOUTが設定されている" do
      let(:params) { { layout: Potepan::CategoriesHelper::GRID_LAYOUT } }
      before do
        allow(view).to receive(:products_layout_grid?).and_return(true)
      end

      it 'グリッドレイアウトで商品が表示される' do
        render template: "potepan/categories/show.html.erb"
        expect(rendered).to have_css('.productGrid')
      end

      it '対象の商品が全て表示されている' do
        render template: "potepan/categories/show.html.erb"
        expect(rendered).to have_content('TAXON1')
        expect(rendered).to have_content('TAXON2')
      end
    end

    context "LIST_LAYOUTが設定されている" do
      let(:params) { { layout: Potepan::CategoriesHelper::LIST_LAYOUT } }
      before do
        allow(view).to receive(:products_layout_grid?).and_return(false)
      end

      it 'リストレイアウトで商品が表示される' do
        render template: "potepan/categories/show.html.erb"
        expect(rendered).to have_css('.productListSingle')
      end

      it '対象の商品が全て表示されている' do
        render template: "potepan/categories/show.html.erb"
        expect(rendered).to have_content('TAXON1')
        expect(rendered).to have_content('TAXON2')
      end
    end
  end

  describe "@taxonでのページタイトルの制御の検証" do
    let(:taxon) { create(:taxon) }

    before do
      assign(:taxonomies, Spree::Taxonomy.none)
      assign(:products, Spree::Product.none)
    end

    context "@taxonが設定されている" do
      it '@taxonの名前がタイトルに表示される' do
        assign(:taxon, taxon)
        render template: "potepan/categories/show.html.erb"
        expect(rendered).to have_css('h2', text: taxon.name)
      end
    end

    context "@taxonが設定されていない" do
      it '"全てのカテゴリー"がタイトルに表示される' do
        render template: "potepan/categories/show.html.erb"
        expect(rendered).to have_css('h2', text: '全てのカテゴリー')
      end
    end
  end

  describe 'カテゴリー一覧のtaxonomy表示の検証' do
    let(:taxonomies) do
      taxon1 = create(:taxon, name: "TAXON1")
      taxon2 = create(:taxon, name: "TAXON2")
      # taxonomyのpositionを登録
      taxonomy1 = taxon1.taxonomy
      taxonomy1.position = 1
      taxonomy1.name = 'TAXONOMY1'
      taxonomy1.save
      taxonomy2 = taxon2.taxonomy
      taxonomy2.position = 2
      taxonomy2.name = 'TAXONOMY2'
      taxonomy2.save

      create(:taxon, name: "TAXON3", taxonomy: taxonomy1)

      Spree::Taxonomy.all
    end

    before do
      assign(:taxonomies, taxonomies)
      assign(:products, Spree::Product.none)
    end

    it 'taxonomy一覧が表示される' do
      render template: "potepan/categories/show.html.erb"
      expect(rendered).to have_css('a.taxonomy', count: 2)
      expect(rendered).to have_content('TAXONOMY1')
      expect(rendered).to have_content('TAXONOMY2')
    end

    it 'taxon一覧が表示される' do
      render template: "potepan/categories/show.html.erb"
      expect(rendered).to have_css('a.taxonomy', count: 2)
      expect(rendered).to have_content('TAXONOMY1')
      expect(rendered).to have_content('TAXONOMY2')
    end
  end

  describe 'カテゴリー一覧のtaxon表示の検証' do
    describe '商品の紐づくtaxonのみ表示されることの検証' do
      let(:taxonomies) do
        taxon1 = create(:taxon, name: "TAXON1")
        taxonomy1 = taxon1.taxonomy
        create(:taxon, name: "TAXON2", taxonomy: taxonomy1)
        product1 = create(:product)
        product2 = create(:product)

        # taxonとproductのひも付きを登録
        classification1 = Spree::Classification.new
        classification1.taxon = taxon1
        classification1.product = product1
        classification1.save

        classification2 = Spree::Classification.new
        classification2.taxon = taxon1
        classification2.product = product2
        classification2.save

        Spree::Taxonomy.all
      end

      before do
        assign(:taxonomies, taxonomies)
        assign(:products, Spree::Product.none)
      end

      it 'TAXON1のみが商品と紐づくため表示対象となる' do
        render template: "potepan/categories/show.html.erb"
        expect(rendered).to have_css('li.taxon a', count: 1, visible: false)
        expect(rendered).to have_content('TAXON1')
        expect(rendered).to have_content('(2)') # TAXON1に紐づく商品数が表示される
        expect(rendered).to_not have_content('TAXON2')
      end

      it 'TAXON1に紐づく商品数が表示される' do
        render template: "potepan/categories/show.html.erb"
        expect(rendered).to have_content('(2)')
      end
    end
  end
end
